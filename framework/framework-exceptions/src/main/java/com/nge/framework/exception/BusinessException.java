package com.nge.framework.exception;

public abstract class BusinessException extends Exception {

	public BusinessException(String msg) {
		super(msg);
	}
	
	private static final long serialVersionUID = 164902756943876422L;

}
