package com.nge.framework.exception;

public class FrameworkSystemException extends SystemException {

	public FrameworkSystemException() {
		super();
	}

	public FrameworkSystemException(Exception cause) {
		super(cause);
	}

	private static final long serialVersionUID = 6707725262471780632L;

}
