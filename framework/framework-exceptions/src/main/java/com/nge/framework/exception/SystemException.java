package com.nge.framework.exception;

public abstract class SystemException extends RuntimeException {

	
	public SystemException(Exception cause) {
		super(cause);
	}

	public SystemException() {
		super();
	}

	private static final long serialVersionUID = -5786340426929655453L;
}
