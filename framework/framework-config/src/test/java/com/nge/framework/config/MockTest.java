package com.nge.framework.config;

public class MockTest {
	
	public static void main(String[] args) {
		System.setProperty("com.nge.framework.mockPropertyA", "Olivia");
		System.setProperty("com.nge.framework.mockPropertyC", "Morgan");
		Mock mock = DynamicConfig.getConfig(Mock.class);
		System.out.println(mock.getMockPropertyA());
		System.out.println(mock.getMockPropertyB());
		System.out.println(mock.getMockPropertyD());
		System.out.println(mock.getMockPropertyE());
	}
}
