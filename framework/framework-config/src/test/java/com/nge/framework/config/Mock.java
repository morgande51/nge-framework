package com.nge.framework.config;

@PropertySourceType(value = SourceType.System)
@SystemPropertiesInfo("com.nge.framework")
@ConfigFilePropertiesInfo(value="C:/temp/mockproperties.properties", rootPrefix="com.nge.framework", internal=false)
public interface Mock {
	
	public String getMockPropertyA();
	
	@PropertyLocation("mockPropertyC")
	public String getMockPropertyB();
	
	@PropertySourceType(SourceType.ConfigFile)
	public String getMockPropertyD();
	
	@PropertySourceType(SourceType.ConfigFile)
	@ConfigFilePropertiesInfo(value="META-INF/mockproperties.properties", rootPrefix="com.nge.framework.test")
	@PropertyLocation("mockPropertyZ")
	public String getMockPropertyE();
	
}
