package com.nge.framework.config;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.nge.framework.exception.FrameworkSystemException;
import com.nge.framework.util.AnnotationDiscoveryUtil;
import com.nge.framework.util.BeanRefelectionUtil;
import com.nge.framework.util.ResourceLoader;
import com.nge.framework.util.SimplePair;

public class DynamicConfig implements InvocationHandler {
	
	public static final String BEAN_GETTER = "get";
	public static final String BEAN_SETTER = "set";
	public static final String PREFIXD_PROPERTY_FORMAT = "%s.%s";
	
	protected static final Map<Class<?>, Object> factoryConfigCache = Collections.synchronizedMap(new HashMap<>());
	
	private SimplePair<SourceType,Annotation> defaultSource;
	private Map<SourceType, Annotation> otherPropertySources;
	private Context context;
//	private Map<Method, String> cache;
	
	public DynamicConfig(Class<?> config) {
		if (!config.isInterface()) {
			throw new FrameworkSystemException();
		}
		seed(config);
//		cache = new Hashtable<Method, String>();
	}
	
	protected void seed(Class<?> clazz) {
		// get all root annotations
		List<Annotation> rootAnnotations = new ArrayList<Annotation>(Arrays.asList(clazz.getAnnotations()));
		PropertySourceType rootPropertyTypeAnn = findAnnotation(PropertySourceType.class, rootAnnotations, true);

		SourceType rootPropertyType = null;
		if (rootPropertyTypeAnn != null) {
			rootPropertyType = rootPropertyTypeAnn.value();
			Annotation rootPropertyInfo = null;
			switch (rootPropertyType) {
				case System:
					SystemPropertiesInfo spi = findAnnotation(SystemPropertiesInfo.class, rootAnnotations, true);
					if (spi == null) {
						throw new FrameworkSystemException();
					}
					rootPropertyInfo = spi;
					break;
					
				case ConfigFile:
					ConfigFilePropertiesInfo cpi = findAnnotation(ConfigFilePropertiesInfo.class, rootAnnotations, true);
					if (cpi == null) {
						throw new FrameworkSystemException();
					}
					rootPropertyInfo = cpi;
					break;
					
				case JNDI:
					JndiPropertiesInfo jpi = findAnnotation(JndiPropertiesInfo.class, rootAnnotations, true);
					if (jpi == null) {
						throw new FrameworkSystemException();
					}
					rootPropertyInfo = jpi;
					break;
					
				default:
					// TODO: Message this
					throw new FrameworkSystemException();
			}
			defaultSource = new SimplePair<SourceType, Annotation>(rootPropertyType, rootPropertyInfo);
		}
		
		// find any additional root property sources
		lazyOtherSources(findAnnotation(SystemPropertiesInfo.class, rootAnnotations, true), SourceType.System);
		lazyOtherSources(findAnnotation(ConfigFilePropertiesInfo.class, rootAnnotations, true), SourceType.ConfigFile);
		lazyOtherSources(findAnnotation(JndiPropertiesInfo.class, rootAnnotations, true), SourceType.JNDI);
		lazyOtherSources(findAnnotation(JndiDatasourcePropertiesInfo.class, rootAnnotations, true), SourceType.Database);
		
		buildInitialContext();
	}

	protected <T> T findAnnotation(Class<T> annotationType, Collection<Annotation> annotations, boolean remove) {
		return AnnotationDiscoveryUtil.filterFind(annotations, annotationType, remove);
	}
	
	protected void lazyOtherSources(Annotation ann, SourceType sourceType) {
		if (otherPropertySources == null) {
			otherPropertySources = new HashMap<SourceType, Annotation>();
		}
		if (ann != null) {
			otherPropertySources.put(sourceType, ann);
		}
	}
	
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		// identify method
		String methodName = method.getName();
		assert methodName.startsWith(BEAN_GETTER) || methodName.startsWith(BEAN_SETTER);
		boolean isGetter = methodName.startsWith(BEAN_GETTER);
		
		// get property
		String property;
		if (method.isAnnotationPresent(PropertyLocation.class)) {
			property = method.getAnnotation(PropertyLocation.class).value();
		}
		else {
			property = BeanRefelectionUtil.getPropertyName(methodName);
		}
		
		// determine appropriate source type and info metadata
		SourceType sourceType = null;
		Annotation info = null;
		if (method.isAnnotationPresent(PropertySourceType.class)) {
			sourceType = method.getAnnotation(PropertySourceType.class).value();
			if (method.isAnnotationPresent(sourceType.getPropertiesInfoType())) {
				info = method.getAnnotation(sourceType.getPropertiesInfoType());
			}
			else if (otherPropertySources.containsKey(sourceType)) {
				info = otherPropertySources.get(sourceType);
			}
		}
		else {
			sourceType = defaultSource.getA();
			info = defaultSource.getB();
		}
		
		Object value = null;
		switch (sourceType) {
			case System:
				if (info != null) {
					String prefix = SystemPropertiesInfo.class.cast(info).value();
					property = String.format(PREFIXD_PROPERTY_FORMAT, prefix, property);
				}
				value = System.getProperty(property);
				break;
				
			case ConfigFile:
				ConfigFilePropertiesInfo cpi;
				if (info != null) {
					cpi = ConfigFilePropertiesInfo.class.cast(info);
				}
				else if (otherPropertySources.containsKey(SourceType.ConfigFile)) {
					info = otherPropertySources.get(SourceType.ConfigFile);
					cpi = ConfigFilePropertiesInfo.class.cast(info);
				}
				else {
					// TODO: message
					throw new FrameworkSystemException();
				}
				Properties p = ResourceLoader.getAsProperties(cpi.value(), cpi.internal());
				if (cpi.rootPrefix().trim().length() > 0) {
					String prefix = cpi.rootPrefix();
					property = String.format(PREFIXD_PROPERTY_FORMAT, prefix, property);
				}
				value = p.getProperty(property);
				break;
				
			case JNDI:
				JndiPropertiesInfo jpi;
				if (info != null) {
					jpi = JndiPropertiesInfo.class.cast(info);
				}
				else if (otherPropertySources.containsKey(SourceType.JNDI)) {
					info = otherPropertySources.get(SourceType.JNDI);
					jpi = JndiPropertiesInfo.class.cast(info);
				}
				else {
					// TODO: message
					throw new FrameworkSystemException();
				}
				String jndiPrefix = jpi.resourceRef() ? "comp/env/" : "";
				String lookupName = "java:/" + jndiPrefix + jpi.value() + "/" + property;
				value = lookupJndiProperity(lookupName);
				break;
				
			default:
				// TODO: message
				throw new FrameworkSystemException();
		}
		/*
		switch (sourceInfoPair.getA()) {
			case System:
				if (sourceInfoPair.getB() != null) {
					SystemPropertiesInfo spi = (SystemPropertiesInfo) sourceInfoPair.getB();
					String prefix = spi.value();
					property = String.format(PREFIXD_PROPERTY_FORMAT, prefix, property);
				}
				value = System.getProperty(property);
				break;
			}
		} */
		
		if (!isGetter) {
			value = method.invoke(proxy, args);
		}
		return value;
	}
	
	protected void buildInitialContext() {
		try {
			context = new InitialContext();
		}
		catch (NamingException e) {
			throw new FrameworkSystemException(e);
		}
	}
	
	protected String lookupJndiProperity(String lookupName) {
		String property;
		try {
			Object o = context.lookup(lookupName);
			if (!(o instanceof String) && !(o instanceof PropertySupplier)) {
				throw new IllegalArgumentException();
			}
			if (o instanceof PropertySupplier) {
				property = PropertySupplier.class.cast(o).getProperty();
			}
			else {
				property = o.toString();
			}
		} 
		catch (NamingException | IllegalArgumentException e) {
			throw new FrameworkSystemException(e);
		}
		return property;
	}
	
	public static <T> T getConfig(Class<T>configClass) {
		if (configClass == null || !configClass.isInterface()) {
			throw new FrameworkSystemException();
		}
		if (!factoryConfigCache.containsKey(configClass)) {
			Object proxy = Proxy.newProxyInstance(configClass.getClassLoader(), 
					new Class[]{configClass}, 
					new DynamicConfig(configClass));
			factoryConfigCache.put(configClass, proxy);
		}
		return configClass.cast(factoryConfigCache.get(configClass));
	}
}