package com.nge.framework.config;

public interface PropertySupplier {

	public String getProperty();
}