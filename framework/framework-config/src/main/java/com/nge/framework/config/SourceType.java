package com.nge.framework.config;

import java.lang.annotation.Annotation;

public enum SourceType {
	System {
		@Override
		public Class<SystemPropertiesInfo> getPropertiesInfoType() {
			return SystemPropertiesInfo.class;
		}
	},
	Database {
		@Override
		public Class<JndiDatasourcePropertiesInfo> getPropertiesInfoType() {
			return JndiDatasourcePropertiesInfo.class;
		}
	},
	ConfigFile {
		@Override
		public Class<ConfigFilePropertiesInfo> getPropertiesInfoType() {
			return ConfigFilePropertiesInfo.class;
		}
	},
	JNDI {
		@Override
		public Class<JndiPropertiesInfo> getPropertiesInfoType() {
			return JndiPropertiesInfo.class;
		}
	};
	
	public abstract Class<? extends Annotation> getPropertiesInfoType();
}
