package com.nge.framework.config;

public @interface JndiDatasourcePropertiesInfo {
	
	String value();
	String statement();
	boolean callable() default false;
}
