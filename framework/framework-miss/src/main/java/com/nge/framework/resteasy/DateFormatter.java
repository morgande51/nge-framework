package com.nge.framework.resteasy;

import java.lang.annotation.Annotation;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.ext.Provider;

import org.jboss.resteasy.spi.StringParameterUnmarshaller;

@Provider
public class DateFormatter implements StringParameterUnmarshaller<Date>{
	
	private SimpleDateFormat sdf;

    public void setAnnotations(Annotation[] annotations) {
       DateFormat format = findDateFormatAnnotation(annotations);
       sdf = new SimpleDateFormat(format.value());
    }

    public Date fromString(String str) {
       try {
          return sdf.parse(str);
       }
       catch (ParseException e) {
          throw new RuntimeException(e);
       }
    }
    
    private DateFormat findDateFormatAnnotation(Annotation[] annotations) {
    	DateFormat df = null;
    	for (Annotation a : annotations) {
    		if (a.annotationType().equals(DateFormat.class)) {
    			df = (DateFormat) a;
    			break;
    		}
    	}
    	return df;
    }
}
