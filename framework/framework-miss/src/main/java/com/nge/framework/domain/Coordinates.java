package com.nge.framework.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class Coordinates implements Serializable {
	
	private static final double EARTH_RADIUS = 6367442.5;
	private static final double MILES_PER_METER = 0.000621371;
	
	@Column(nullable=false, precision=10, scale=7)
	private Double latitude;

	@Column(nullable=false, precision=10, scale=7)
	private Double longitude;

	/**
	 * Constructor
	 */
	public Coordinates() {
		super();
	}
	
	/**
	 * Constructor
	 * @param latitude the latitude to set
	 * @param longitude the longitude to set
	 */
	public Coordinates(double latitude, double longitude) {
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	
	public double getDistance(double lat, double lng) {
		double dLat = Math.toRadians(latitude.doubleValue() - lat);
		double dLong = Math.toRadians(longitude.doubleValue() - lng);
		double x = Math.pow((Math.sin(dLat / 2)), 2) + 
				   Math.cos(Math.toRadians(latitude.doubleValue())) *
				   Math.cos(Math.toRadians(lat)) *
				   Math.pow((Math.sin(dLong / 2)), 2);
		double y = 2 * Math.asin(Math.sqrt(x));
		double distanceInMeters = EARTH_RADIUS * y;
		return distanceInMeters * MILES_PER_METER;
	}
	
	private static final long serialVersionUID = 7815624891543920088L;

}