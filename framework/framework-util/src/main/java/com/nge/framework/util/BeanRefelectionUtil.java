package com.nge.framework.util;

public class BeanRefelectionUtil {

	private static final String BEAN_GETTER = "get";
	private static final String BEAN_SETTER = "set";
	private static String propertyFormat = "%s%s";
	
	public static String getPropertyName(String methodName) {
		assert methodName.startsWith(BEAN_GETTER) || methodName.startsWith(BEAN_SETTER);
		String propertyName = methodName.substring(3);
		String lowerHump = propertyName.substring(0, 1).toLowerCase();
		propertyName = propertyName.substring(1);
		return String.format(propertyFormat, lowerHump, propertyName);
	}
}
