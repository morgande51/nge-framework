/**
 * 
 */
package com.nge.framework.util;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.KeySpec;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * @author morgande1
 *
 */
public class AESEncrypter {

	private static final byte[] SALT = {
        (byte) 0xA9, (byte) 0x9B, (byte) 0xC8, (byte) 0x32,
        (byte) 0x56, (byte) 0x35, (byte) 0xE3, (byte) 0x03
    };
	
	private static final String AES = "AES";
	private static final String DEFAULT_AES_ALGORITHM = "PBKDF2WithHmacSHA1";
	private static final String DEFAULT_TRANSFORMATION = "AES/CBC/PKCS5Padding";
	private static final String DEFAULT_CHARSET = "UTF8";
	
    private static final int ITERATION_COUNT = 65536;
    private static final int KEY_LENGTH = 256;
    private Cipher ecipher;
    private Cipher dcipher;
    
    public AESEncrypter(String passPhrase) throws 
    	NoSuchAlgorithmException, 
    	InvalidKeySpecException, 
    	NoSuchPaddingException, 
    	InvalidKeyException, 
    	InvalidParameterSpecException, 
    	InvalidAlgorithmParameterException
    {
        SecretKeyFactory factory = SecretKeyFactory.getInstance(DEFAULT_AES_ALGORITHM);
        KeySpec spec = new PBEKeySpec(passPhrase.toCharArray(), SALT, ITERATION_COUNT, KEY_LENGTH);
        SecretKey tmp = factory.generateSecret(spec);
        SecretKey secret = new SecretKeySpec(tmp.getEncoded(), AES);
 
        ecipher = Cipher.getInstance(DEFAULT_TRANSFORMATION);
        ecipher.init(Cipher.ENCRYPT_MODE, secret);
       
        dcipher = Cipher.getInstance(DEFAULT_TRANSFORMATION);
        byte[] iv = ecipher.getParameters().getParameterSpec(IvParameterSpec.class).getIV();
        dcipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(iv));
    }
    
    public String encrypt(String encrypt) throws UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {
        byte[] bytes = encrypt.getBytes(DEFAULT_CHARSET);
        byte[] encrypted = encrypt(bytes);
//        return new BASE64Encoder().encode(encrypted);
        return Base64.encodeBase64String(encrypted);
    }
 
    public byte[] encrypt(byte[] plain) throws IllegalBlockSizeException, BadPaddingException {
        return ecipher.doFinal(plain);
    }
 
    public String decrypt(String encrypt) throws UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {
//        byte[] bytes = new BASE64Decoder().decodeBuffer(encrypt);
    	byte[] bytes = Base64.decodeBase64(encrypt);
        byte[] decrypted = decrypt(bytes);
        return new String(decrypted, DEFAULT_CHARSET);
    }
 
    public byte[] decrypt(byte[] encrypt) throws IllegalBlockSizeException, BadPaddingException {
        return dcipher.doFinal(encrypt);
    }
    
    public static void main(String[] args) throws Exception {
    	String password = "09126266";
    	String secret = "8f485259b06af566ee51d6c72ad35119";
    	AESEncrypter encrypter = new AESEncrypter(secret);
    	String encryptedPwd = encrypter.encrypt(password);
    	System.out.println("Password encrypted: " + encryptedPwd);
    	System.out.println("Password decrypted: " + encrypter.decrypt(encryptedPwd));
    }
}