package com.nge.framework.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import com.nge.framework.exception.FrameworkSystemException;

public class ResourceLoader {
	
	private ResourceLoader() {}

	public static Properties getAsProperties(String name) {
		Properties props = null;
		URL url = ResourceLoader.getAsUrl(name);
	
		if (url == null) {
			throw new FrameworkSystemException();
		}
		try {
			props = new Properties();
			props.load(url.openStream());
		} 
		catch (IOException e) {
			// TODO: handle this
			throw new FrameworkSystemException();
		}

		return props;
	}
	
	public static Properties getAsProperties(String name, boolean internal) {
		Properties props = null;
		if (!internal) {
			FileInputStream fio = null;
			try {
				fio = new FileInputStream(new File(name));
				props = new Properties();
				props.load(fio);
				fio.close();
			} 
			catch (IOException e) {
				// TODO: handle this
				throw new FrameworkSystemException();
			}
			finally {
				if (fio != null) {
					try {
						fio.close();
					} 
					catch (IOException e) {
						// TODO Auto-generated catch block
						throw new FrameworkSystemException();
					}
				}
			}
		}
		else {
			props = getAsProperties(name);
		}

		return props;
	}

	public static URL getAsUrl(String name) {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		URL resource = classLoader.getResource(name);
		if (resource == null) {
			classLoader = ResourceLoader.class.getClassLoader();
			resource = classLoader.getResource(name);
		}
		return classLoader.getResource(name);
	}
}