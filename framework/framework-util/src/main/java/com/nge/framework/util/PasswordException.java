package com.nge.framework.util;

import com.nge.framework.exception.BusinessException;

public class PasswordException extends BusinessException {

	public PasswordException(String msg) {
		super(msg);
	}

	private static final long serialVersionUID = -451824046668648076L;
}