/**
 * 
 */
package com.nge.framework.util;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

/**
 * @author Darnell Morgan
 *
 */
public class DigitalSignatureUtil {
	
	public static final String DEFAULT_ALGORITHM = "HmacSHA256";
	public static final String PLUS = "+";
	public static final String DASH = "-";
	public static final String UNDERSCORE = "_";
	public static final String SLASH = "/";
	
	/*
	public static String sign(String serializedData, String secret) throws IOException, NoSuchAlgorithmException, InvalidKeyException {		
		Mac mac = Mac.getInstance(DEFAULT_ALGORITHM);
	    SecretKeySpec secretKey = new SecretKeySpec(secret.getBytes(), mac.getAlgorithm());
	    mac.init(secretKey);
	    byte[] digest = mac.doFinal(serializedData.getBytes());
	    return Base64.encodeBase64URLSafeString(digest);
	}
	*/
	public static String sign(String secret, Serializable data) throws IOException, NoSuchAlgorithmException, InvalidKeyException {		
		byte[] base64EncData = Base64SerializationUtil.serializeToBinary(data);
		Mac mac = Mac.getInstance(DEFAULT_ALGORITHM);
	    SecretKeySpec secretKey = new SecretKeySpec(secret.getBytes(), mac.getAlgorithm());
	    mac.init(secretKey);
	    byte[] digest = mac.doFinal(base64EncData);
	    return Base64.encodeBase64URLSafeString(digest);
	}
	
	public static boolean checkSignedSignature(String encodedSig, String serializedData, String secret) throws IOException, NoSuchAlgorithmException, InvalidKeyException {
		Base64 base64 = new Base64(true);
		
		Mac mac = Mac.getInstance(DEFAULT_ALGORITHM);
		byte[] expectedSig = base64.decode(urlDecode(encodedSig).getBytes());
		SecretKeySpec secretKeySpec = new SecretKeySpec(secret.getBytes(), mac.getAlgorithm());
		mac.init(secretKeySpec);
		byte[] actualSig = mac.doFinal(serializedData.getBytes());
		return Arrays.equals(expectedSig, actualSig);
	}
    
	public static String urlDecode(String url) {
		return url.replaceAll(DASH, PLUS).replaceAll(UNDERSCORE, SLASH);		
	}
	
	public static String urlEncode(String url) throws UnsupportedEncodingException {
//		return url.replaceAll(PLUS, DASH).replaceAll(SLASH, UNDERSCORE);
		return URLEncoder.encode(url, "UTF-8");
	}
	
	public static void main(String[] args) throws Exception {
		String data = "d73TLykJQeD25gYGLcdqqDjdSibtBku6SzAPkn59IXs.eyJhbGdvcml0aG0iOiJITUFDLVNIQTI1NiIsImNvZGUiOiIyLkFRQ1hRWk9SNUkya1ZYUDkuMzYwMC4xMzM2NjA4MDAwLjEtNTAwMzUyNTU4fHNUV0pGY0xRX1l5Z2JaOElYdjB6TVNBbm5qOCIsImlzc3VlZF9hdCI6MTMzNjYwMzk1NSwidXNlcl9pZCI6IjUwMDM1MjU1OCJ9";
		String[] payload = data.split("\\.");
		System.out.println(checkSignedSignature(payload[0], payload[1], "8f485259b06af566ee51d6c72ad35119"));
		System.out.println(Base64SerializationUtil.encodeToBase64("1024".getBytes()));
	}
}