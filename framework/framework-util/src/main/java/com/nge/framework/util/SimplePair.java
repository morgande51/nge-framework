package com.nge.framework.util;

import java.io.Serializable;

public class SimplePair<A,B> implements Serializable {
	
	private A a;
	
	private B b;
	
	public SimplePair(A a, B b) {
		this.a = a;
		this.b = b;
	}
	
	public A getA() {
		return a;
	}
	
	public void setA(A a) {
		this.a = a;
	}
	
	public B getB() {
		return b;
	}
	
	public void setB(B b) {
		this.b = b;
	}
	
	private static final long serialVersionUID = 3578984271311436097L;
}