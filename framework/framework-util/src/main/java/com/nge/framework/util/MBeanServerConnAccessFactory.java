package com.nge.framework.util;

import java.util.EnumSet;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import javax.management.MBeanServerConnection;
import javax.management.MBeanServerFactory;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import com.nge.framework.exception.FrameworkSystemException;

public class MBeanServerConnAccessFactory {
	
	public static final AccessType MBEAN_SERVER_NATIVE_ACCESS_TYPE = AccessType.get(0);
	public static final AccessType MBEAN_SERVER_JNDI_ACCESS_TYPE = AccessType.get(1);
	
	private static final String MBEAN_SERVER_ACCESS_TYPE = "%s.nge.framework.mbeanServer.accessType";
	private static final String MBEAN_SERVER_JNDI_NAME = "%s.nge.framework.mbeanServer.jndiName";
	private static final String MBEAN_SERVER_AGENT_ID = "%s.nge.framework.mbeanServer.agentId";
	private static final String MBEAN_SERVER_INDEX = "%s.nge.framework.mbeanServerArray.index";
	
	// defaults
	private static final String DEFAULT_APP_NAME = "default";
	private static final String DEFAULT_AGENT_ID = "none";
	private static final String DEFAULT_INDEX = "0";
//	private static final String DEFAULT_TYPE = MBEAN_SERVER_NATIVE_ACCESS_TYPE.name();
	
	// set defaults
	private static void initDefaultProperties() {
		Properties systemProperties = System.getProperties();
		setDefaultProperty(systemProperties, MBEAN_SERVER_ACCESS_TYPE, MBEAN_SERVER_NATIVE_ACCESS_TYPE.name());
		setDefaultProperty(systemProperties, MBEAN_SERVER_AGENT_ID, DEFAULT_AGENT_ID);
		setDefaultProperty(systemProperties, MBEAN_SERVER_INDEX, DEFAULT_INDEX);
	}
	
	private static void setDefaultProperty(Properties systemProperties, String propertyName, String value) {
		String key = String.format(propertyName, DEFAULT_APP_NAME);
		if (!systemProperties.containsKey(key)) {
			System.setProperty(key, value);
		}
	}
	
	static {
		initDefaultProperties();
	}
	
	private static final MBeanServerConnAccessFactory singleton = new MBeanServerConnAccessFactory();
	
	/*
	public static <T> T getConfig(Class<T> configClass, String jmxServiceName) {
		T config = null;
		MBeanServer server = MBeanServerFactory.findMBeanServer(getAgentId()).get(getServerIndex());
//		server.get
//		MBeanProxyE
		return config;
	} /*
	
	/**
	 * Hidden Constructor for singleton purposes
	 */
	protected MBeanServerConnAccessFactory() {}
	
	public MBeanServerConnection getServer() {
		return getServer(DEFAULT_APP_NAME);
	}
	
	public MBeanServerConnection getServer(String appName) {
		AccessType accessType = AccessType.get(getAccessType(appName));
		return accessType.getServerFromConfig(this, appName);
	}
	
	public MBeanServerConnection getServerNative(String appName) {;
		return MBeanServerFactory.findMBeanServer(getAgentId(appName)).get(getServerIndex(appName));
	}
	
	public MBeanServerConnection getServerJNDI(String appName) {
		MBeanServerConnection serverConn = null;
		try {
			Context c = new InitialContext();
			serverConn = (MBeanServerConnection) c.lookup(getJNDIName(appName));
		} 
		catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return serverConn;
	}
	
	public MBeanServerConnection getServerJNDI(String jndiName, String jndiProperties) {
		MBeanServerConnection serverConn = null;
		try {
			Context c = (jndiProperties == null) ? new InitialContext() : getContext(jndiProperties);
			serverConn = (MBeanServerConnection) c.lookup(jndiName);
		} 
		catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return serverConn;
	}
	
	public MBeanServerConnection getServerJNDI(String jndiName, Hashtable<?, ?> jndiProperties) {
		MBeanServerConnection serverConn = null;
		try {
			Context c = (jndiProperties == null) ? new InitialContext() : getContext(jndiProperties);
			serverConn = (MBeanServerConnection) c.lookup(jndiName);
		} 
		catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return serverConn;
	}
	
	private Context getContext(String jndiPropertiesName) throws NamingException {
		return getContext(ResourceLoader.getAsProperties(jndiPropertiesName));
	}
	
	private Context getContext(Hashtable<?, ?> jndiProperties) throws NamingException {
		return new InitialContext(jndiProperties);
	}
	
	protected String getJNDIName(String appName) {
		String agentId = null;
		try {
			agentId = getProperty(MBEAN_SERVER_JNDI_NAME, appName);
		}
		catch (Exception e) {
			// TODO: exception message for fail to get jndiName from System Property
			throw new FrameworkSystemException();
		}
		return agentId;
	}
	
	protected String getAgentId(String appName) {
		String agentId;
		try {
			String propertyKey = getProperty(MBEAN_SERVER_AGENT_ID, appName);
			agentId = propertyKey.equals(DEFAULT_AGENT_ID) ? null : propertyKey;
		}
		catch (Exception e) {
			// TODO: notify logging mechanism that we are using default
			agentId = null;
		}
		return agentId;
	}
	
	protected int getServerIndex(String appName) {
		int index;
		try {
			String propertyKey = getProperty(MBEAN_SERVER_INDEX, appName);
			index = Integer.parseInt(propertyKey);
		}
		catch (Exception e) {
			// TODO: notify logging mechanism that we are using default
			index = 0;
		}
		return index;
	}
	
	protected int getAccessType(String appName) {
		int type;
		try {
			String accessTypeValue = getProperty(MBEAN_SERVER_ACCESS_TYPE, appName);
			type = Integer.parseInt(accessTypeValue);
		}
		catch (Exception e) {
			// TODO: notify logging mechanism that we are using default
			type = MBEAN_SERVER_NATIVE_ACCESS_TYPE.getType();
		}
		return type;
	}
	
	private String getProperty(String appName, String propertyName) {
		String property = null;
		try {
			String propertyKey = String.format(propertyName, appName);
			property = System.getProperty(propertyKey);
		}
		catch (Exception e) {
			// TODO: Log this error;
		}
		return property;
	}
	
	public static final MBeanServerConnAccessFactory getDefaultInstance() {
		return singleton;
	}
	
	enum AccessType {
		NATIVE(0) {
			@Override
			protected MBeanServerConnection getServerFromConfig(MBeanServerConnAccessFactory config, String appName) {
				return config.getServerNative(appName);
			}
		},
		JNDI(1) {
			@Override
			protected MBeanServerConnection getServerFromConfig(MBeanServerConnAccessFactory config, String appName) {
				return config.getServerJNDI(appName);
			}
		};
		
		private static final Map<Integer, AccessType> lookup = new Hashtable<Integer, AccessType>();
		
		static {
			for (AccessType t : EnumSet.allOf(AccessType.class)) {
				lookup.put(t.getType(), t);
			}
		}
		
		private int type; 
		
		private AccessType(int type) {
			this.type = type;
		}
		
		public int getType() {
			return type;
		}
		
		public static AccessType get(int type) {
			return lookup.get(type);
		}
		
		protected abstract MBeanServerConnection getServerFromConfig(MBeanServerConnAccessFactory config, String appName);
	}
}
