package com.nge.framework.util;

import java.lang.annotation.Annotation;
import java.util.Collection;

public class AnnotationDiscoveryUtil {
	
	@SuppressWarnings("unchecked")
	public static <T> T filterFind(Annotation[] annotations, Class<T> classToFind) {
		T foundAnnotation = null;
		for (Annotation a : annotations) {
			Class<? extends Annotation> clazz = a.annotationType();
			if (clazz.isAssignableFrom(classToFind)) {
				foundAnnotation = (T) a;
			}
		}
		return foundAnnotation;
	}
	
	public static <T> T filterFind(Collection<Annotation> annotations, Class<T> classToFind, boolean remove) {
		Annotation foundAnnotation = null;
		for (Annotation a : annotations) {
			Class<? extends Annotation> clazz = a.annotationType();
			if (clazz.isAssignableFrom(classToFind)) {
				foundAnnotation = a;
			}
		}
		if (foundAnnotation != null && remove) {
			annotations.remove(foundAnnotation);
		}
		return classToFind.cast(foundAnnotation);
	}
}
