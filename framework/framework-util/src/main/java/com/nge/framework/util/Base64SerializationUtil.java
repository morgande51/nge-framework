package com.nge.framework.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.apache.commons.codec.binary.Base64;

public class Base64SerializationUtil {

	public static String encodeToBase64(byte[] data) {
		return Base64.encodeBase64URLSafeString(data);
	}
	
	public static byte[] decodeBase64(String data) {
		return Base64.decodeBase64(data);
	}
	
	public static byte[] serializeToBinary(Serializable data) throws IOException {
		// serialize the data first
		ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		ObjectOutputStream out = new ObjectOutputStream(buffer);
		out.writeObject(data);
		out.flush();
		out.close();
		return buffer.toByteArray();
	}
	
	public static String serializeAndEncodeToBase64(Serializable data) throws IOException {
		return encodeToBase64(serializeToBinary(data));
	}
}
