package com.nge.framework.util;

import static org.junit.Assert.*;

import org.junit.Test;

public class PasswordUtilTest {

	@Test
	public void testAuthenticate() throws Exception {
		String password = "TestPassword";
		byte[] salt = PasswordUtil.generateSalt();
		byte[] encPwd = PasswordUtil.getEncryptedPassword(password, salt);
		assertTrue(PasswordUtil.authenticate(password, encPwd, salt));
	}
	
	@Test
	public void testGenerateTempPassword() throws Exception {
		String temp = PasswordUtil.generatePassword(8, 8, 1, 1, 1);
		System.out.println(temp);
		assertNotNull(temp);
	}
}
