/**
 * 
 */
package com.nge.framework.cache;

import java.io.Serializable;

/**
 * @author Darnell Morgan
 *
 */
public interface NGECacheAware {

	public void setCacheKey(Serializable key);
	public Serializable getCacheKey();
}
