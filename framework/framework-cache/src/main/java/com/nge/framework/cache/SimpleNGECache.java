/**
 * 
 */
package com.nge.framework.cache;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Darnell Morgan
 */
public class SimpleNGECache implements NGECache {
	
	private static final NGECache singleton = new SimpleNGECache();
	
	private Map<String, Map<Serializable, ?>> cacheRegions;
	
	public SimpleNGECache() {
		cacheRegions = new HashMap<String, Map<Serializable,?>>();
	}

	public <T> T getObj(String cacheName, Serializable key, Class<T> objClass) {
		Map<Serializable, ?> cache = cacheRegions.get(cacheName);
		T value = null;
		if (cache == null) {
			System.out.println("No cache found for region: " + cacheName);
		}
		else {
			System.out.println("Cache: " + cacheName + " has object for key: " + key + " = " + cache.keySet().contains(key));
			Object obj = cache.get(key);
			value = objClass.cast(obj);
		}
		return value;
	}
	
	public void putObj(String cacheName, Serializable key, Object value) {
		@SuppressWarnings("unchecked")
		Map<Serializable, Object> cache = (Map<Serializable, Object>) cacheRegions.get(cacheName);
		if (cache == null) {
			cache = new HashMap<Serializable, Object>();
			cacheRegions.put(cacheName, cache);
		}
		System.out.println("Putting object into cache: " + cacheName + " with key: " + key);
		cache.put(key, value);
	}
	
	public <T> T removeObj(String cacheName, Serializable key, Class<T> objClass) {
		Map<Serializable, ?> cache = cacheRegions.get(cacheName);
		T value = null;
		if (cache != null) {
			Object obj = cache.remove(key);
			value = objClass.cast(obj);
		}
		return value;		
	}
	
	// TODO: This should be loaded via META-INF/services plugin
	public static NGECache defaultCache() {
		return singleton;
	}

}