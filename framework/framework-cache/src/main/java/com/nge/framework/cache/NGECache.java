/**
 * 
 */
package com.nge.framework.cache;

import java.io.Serializable;

/**
 * @author Darnell Morgan
 */
public interface NGECache {

	public <T> T getObj(String cache, Serializable key, Class<T> objClass);
	public void putObj(String cache, Serializable key, Object value);
	public <T> T removeObj(String cacheName, Serializable key, Class<T> objClass);
}
