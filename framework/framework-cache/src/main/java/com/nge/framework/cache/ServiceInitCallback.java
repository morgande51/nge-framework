/**
 * 
 */
package com.nge.framework.cache;


/**
 * @author Darnell Morgan
 *
 */
public interface ServiceInitCallback<T> {
	
	public NGECacheAware lazyLoad();
}
